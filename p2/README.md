> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Course LIS4368

## Tyneria Cobbins

### Project 2 Requirements:


1. Provide Create, Read, Update, and Delete Functionalities
2. Create/Modify Servlet Files
3. Compile Class and Servlet Files


#### README.md file should include the following items:

1. Pre-valid user form entry
2. Post-valid user form entry
3. MySQL customer table entry

*Screenshot Pre Valid*:
![Pre-valid Screenshot](img/pre.png)


*Screenshot Post Valid*:
![Post-valid Screenshot](img/post.png)


*Screenshot*:
![Screenshot](img/pic1.png)


*Screenshot*:
![Screenshot](img/pic2.png)


*Screenshot*:
![Screenshot](img/pic3.png)


*Screenshot Delete Warning*:
![Post-valid Screenshot](img/pic4.png)


*Screenshot MySQL*:
![MySQL Screenshot](img/mysql.png)

> This is a blockquote.



#### Assignment Screenshots:








