> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4368

## Tyneria Cobbins

### Project #1 Requirements:

*Sub-Heading:*

1.   Insert/Add JQuery Validations 
2.   Insert/Add regular expressions


#### README.md file should include the following items:

* Screenshot of Failed Validation Form
* Screenshot of Passed validation Form
* Project 1 Index


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of Failed Validation:

![Failed Validation Screenshot](img/try1.png)

*Screenshot of Passed Validation*:

![Passed Validation Screenshot](img/pass.png)

*Screenshot of Project 1 Index*:

![Project 1 Index Screenshot](img/pindex.png)



