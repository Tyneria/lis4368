> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4368 Web Application Development

## Tyneria Cobbins

LIS4368 Requirements

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Tomcat
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorials (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Install MySQL Workbench
    - Write a "Hello-world" Java Servlet
    - Write a Datatbase Servlet

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create a Entity Relationship Diagram
    - Forward Engineer Entity Relationship Diagram
    - Provide Screenshots of ERD and a3 index

4. [P1 README.md](p1/README.md "My P1 README.md file")
    - Insert/Add JQuery validations
    - Insert/Add Regular Expressions
    - Provide Screenshots of the Validations and p1 index


5. [A4 README.md](a4/README.md "My A4 README.md file")
    - Insert/Add Regular Expression
    - Compile Servlet Files 
    - Provide Screenshots of Validations and a4 index

6. [A5 README.md](a5/README.md "My A5 README.md file")
    - Create Servlet Packages
    - Create and Compile Servlet Files
    - Provide Screenshots of Validations and Forms

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Provide Create, Read, Update, and Delete Functionalities
    - Create/Modify Servlet Files
    - Compile Class and Servlet Files
    - Provide Screenshots of Validations and Forms