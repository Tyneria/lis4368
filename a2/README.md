> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Course LIS4368

## Tyneria Cobbins

### Assignment #2 Requirements:

*Course Work Links:*

1. Install MySQL Workbench 
2. Write a "Hello-world" Java Servlet
3. Write a Database Servlet

#### README.md file should include the following items:

* Screenshot of https://localhost9999/hello running
* Screenshot of https://localhost9999/hello/HelloHome.html running
* Screenshot of https://localhost9999/hello/sayhello running
* Screenshot of https://localhost9999/hello/querybook.html running

> This is a blockquote.
>


#### Assignment Screenshots:



![Screenshot 1](img/index.png)


![Screenshot 2](img/query.png)


![Screenshot 3](img/queryresults.png)


![Screenshot 4](img/sayhello.png)


![Screenshot 5](img/site.png)



