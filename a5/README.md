> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Tyneria Cobbins

### Assignment #5 Requirements:

*Sub-Heading:*

1. Create Servlet Packages
2. Create Servlet Files
3. Compile Servlet Files

#### README.md file should include the following items:

* Screenshot of Valid User Form Entry
* Screenshot of Passed Validation Form
* Database Entry Screenshot


> This is a blockquote.
> 


#### Assignment Screenshots:

*Screenshot of VUFE*:

![Valid User Form Entry Screenshot](img/vufe.png)

*Screenshot of Passed Validation*:

![Passed Validation Screenshot](img/pv.png)

*Screenshot of Database Entry*:

![Database Entry Screenshot](img/db.png)


