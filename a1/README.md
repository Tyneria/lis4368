> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Course LIS4368

## Tyneria Cobbins

### Assignment #1 Requirements:

*Three Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Java/JSP/Servlet Development Installation
3. Chapter Questions (Chs 1-4)

#### README.md file should include the following items:

* Screenshot of running java Hello (#1 above);
* Screenshot of running https://localhost9999 (#2 above, Step #4(b) in tutorial);
* Git commands with short descriptions
* Bitbucket repo links a) this assignment and the completed tutorial above (bitbucketstationlocations).

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

* Git init: Create an empty Git repository or reinitialize an existing one
* Git status: Show the working tree status
* Git add: Add file contents to the index
* Git commit: Saving changes
* Git push: Update remote refs along with associated objects
* Git pull: Fetch from and integrate with another repository or a local branch
* Git merge: Run a merge for files needing merging


#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Tomcat*:

![Tomcat Installation Screenshot](img/tomcat.png)

*Screenshot of Index*:

![Portfolio Screenshot](img/index.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/Tyneria/bitbucketstationlocations/ "Bitbucket Station Locations")
