> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Course LIS4368

## Tyneria Cobbins

### Assignment #3 Requirements:

*Deliverables:*

1. Entity Relationship Diagram (ERD)
2. Include data (at least 10 records for each table)
3. Provide Bitbucket read-only access to repo (Language SQL).
    * docs folder: a3.mwb and a3.sql
    * img folder: a3.png (export a3.mwb as a3.png)
    * README.md (*MUST* display a3.png ERD)

#### README.md file should include the following items:

* Screenshot of ERD that links to the image:

*Screenshot A3 ERD*:
![A3 ERD Screenshot](img/a3.png)


*Screenshot A3 Index*:
![A3 Index Screenshot](img/index.png)

> This is a blockquote.



#### Assignment Screenshots:





*A3 docs: a3.mwb and a3.sql*:

[A3 MWB File](docs/a3.mwb)

[A3 SQL File](docs/a3.sql)




