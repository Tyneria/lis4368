> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4368

## Tyneria Cobbins

### Assignment 4 Requirements:



1. Construct Files
2. Insert/Add Regular Expression
3. Compile Servlet Files

#### README.md file should include the following items:

* Screenshot of Failed Validation
* Screenshot of Passed Validation
* Index Screenshot


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>


#### Assignment Screenshots:

*Screenshot of Failed Validation*:

![Failed Screenshot](img/fail.png)

*Screenshot of Passed Validation*:

![Passed Screenshot](img/pass1.png)

*Screenshot of Index*:

![Index Screenshot](img/index.png)



